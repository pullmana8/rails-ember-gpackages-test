// for more details see: http://emberjs.com/guides/models/defining-models/

BlogApi.Change = DS.Model.extend({
  version: DS.attr('string'),
  useflag: DS.attr('string')
});
