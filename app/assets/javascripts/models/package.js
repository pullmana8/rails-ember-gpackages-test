// for more details see: http://emberjs.com/guides/models/defining-models/

BlogApi.Package = DS.Model.extend({
  category: DS.attr('string'),
  name: DS.attr('string'),
  nameSort: DS.attr('string'),
  atom: DS.attr('string'),
  description: DS.attr('string'),
  longdescription: DS.attr('string'),
  homepage: DS.attr('string'),
  license: DS.attr('string'),
  licenses: DS.attr('string'),
  herds: DS.attr('string'),
  maintainers: DS.attr('array'),
  useflags: DS.attr('hash'),
  metadataHash: DS.attr('string')
});
