// For more information see: http://emberjs.com/guides/routing/

BlogApi.Router.map(function() {
  this.resource('categories');
  this.resource('packages');
});
