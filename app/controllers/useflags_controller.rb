class UseflagsController < ApplicationController
  before_action :set_useflag, only: [:show, :update, :destroy]

  # GET /useflags
  def index
    @useflags = Useflag.all

    render json: @useflags
  end

  # GET /useflags/1
  def show
    render json: @useflag
  end

  # POST /useflags
  def create
    @useflag = Useflag.new(useflag_params)

    if @useflag.save
      render json: @useflag, status: :created, location: @useflag
    else
      render json: @useflag.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /useflags/1
  def update
    if @useflag.update(useflag_params)
      render json: @useflag
    else
      render json: @useflag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /useflags/1
  def destroy
    @useflag.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_useflag
      @useflag = Useflag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def useflag_params
      params.fetch(:useflag, {})
    end
end
