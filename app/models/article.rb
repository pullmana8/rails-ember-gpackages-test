require 'elasticsearch/model'

class Article < ApplicationRecord
  include Elasticsearch::Model

  index_name    "articles"
  document_type "article"

  def as_indexed_json(options={})
    as_json(only: 'title')
  end
end

Article.create(title: 'Elasticsearch and Rails', isbn: Random.rand(8888), price: Random.rand(100), author: 'Clement')
