class CreateChanges < ActiveRecord::Migration[5.2]
  def change
    create_table :changes do |t|
      t.string :package
      t.string :category
      t.string :change_type
      t.string :version
      t.string :arches
      t.string :commit

      t.timestamps
    end
  end
end
