# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_01_021215) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.string "author"
    t.float "price"
    t.integer "isbn"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.text "metadata_hash"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "changes", force: :cascade do |t|
    t.string "package"
    t.string "category"
    t.string "change_type"
    t.string "version"
    t.string "arches"
    t.string "commit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "packages", force: :cascade do |t|
    t.string "category"
    t.string "name"
    t.string "name_sort"
    t.string "atom"
    t.text "description"
    t.text "longdescription"
    t.string "homepage"
    t.string "license"
    t.string "licenses"
    t.string "herds"
    t.string "maintainers"
    t.text "useflags"
    t.string "metadata_hash"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "useflags", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "atom"
    t.string "scope"
    t.string "use_expand_prefix"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "versions", force: :cascade do |t|
    t.string "version"
    t.string "package"
    t.string "atom"
    t.integer "sort_key"
    t.string "slot"
    t.string "subslot"
    t.string "eapi"
    t.string "keywords"
    t.string "masks"
    t.string "use"
    t.string "restrict"
    t.string "properties"
    t.string "metadata_hash"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
