Rails.application.routes.draw do
  resources :useflags
  resources :versions
  resources :changes
  resources :packages
  resources :categories
  resources :articles

  root 'ember#bootstrap'
  get '/*path' => 'ember#bootstrap'
end
